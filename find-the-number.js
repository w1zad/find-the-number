/**
 * Find the number in the sequence
 * 
 * What is the 15th element of the sequent?
 * 1220
 * 
 * What is your solution time and space complexity, regarding for the n(th) element of the sequent?
 * It has linear complexity of O(n)
 * 
 * What clean code principles you have been used, and why?
 * I wanted to keep it as simple as possible to gain speed
 * 
 * @param {number} n
 * @param {number} [a=1]
 * @returns {number}
 */ 
function findNumberInSequence(n, a = 1) {
    var prev;
    var b = 0;

    while (n--) {
        prev = a;
        a += b;
        b = prev;
    }
    return b;
}

/**
 * 
 * Find the number in the sequence recursive
 * 
 * What is the 15th element of the sequent?
 * 1220
 * 
 * What is your solution time and space complexity, regarding for the n(th) element of the sequent?
 * It has a very high complexity of O(2^n). 
 * 
 * What clean code principles you have been used, and why?
 * I wanted to keep it as simple as possible to gain speed. In this case it was not possible. So I added a comment in the code.
 * 
 * @param {number} n
 * @param {number} [base=1]
 * @param {number[]} [buff=[]]
 * @returns {number[]}
 */
function findNumberInSequenceRecursiveStyle(n, base = 1, buff = []) {
    if (n <= base)
        return base;

    // Only calculate if needed
    if (buff[n] == undefined) {
        buff[n] = findNumberInSequenceRecursiveStyle(n - 1, base, buff) + findNumberInSequenceRecursiveStyle(n - 2, base, buff);
    }

    return buff[n];
}

/**
 * Find the number in the sequence - recursive style
 * 
 * Complexity O(2^n) unmodified
 * 
 * 
 * @param {number} n
 * @param {number} [base=1]
 * @returns {number}
 */
function findNumberInSequenceRecursiveStyleNormal(n, base = 1) {
    if (n <= base)
        return base;

    return findNumberInSequenceRecursiveStyleNormal(n - 1, base) + findNumberInSequenceRecursiveStyleNormal(n - 2, base);
}